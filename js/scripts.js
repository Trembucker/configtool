$(document).ready(function(){
    $(window).scroll(function() { // check if scroll event happened
      if ($(document).scrollTop() > 650) { // check if user scrolled more than jumbotron height from top of the browser window
        $("#topNavbar").css("background-color", "rgb(73, 78, 107)"); // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
        
      } else {
        $("#topNavbar").css("background-color", "transparent"); // if not, change it back to transparent
      }
    });
    $('.map-area').click(function(){
      $('#shapeModal').modal('show');
    })
  });